using UnityEngine;
using UnityEngine.InputSystem;

public class Character2 : MonoBehaviour
{
    public CharSound charSound;
    public InputActionAsset inputActions;
    public InputAction moveAction;
    public InputAction camTurnLAction;
    public InputAction camTurnRAction;
    public InputAction jumpAction;
    public float moveSpeed;
    public float rotSpeed;
    public Transform meshTransform;
    public Transform camHolderTransform;
    public CharacterController cc;
    public float vSpeed;
    public float gravity = 9.81f;
    public float friction = 2f;
    public CCState currentState;
    public CCState normalState;
    public CCState rotationState;
    public float camRotAmount;
    public float camRotDuration = 0.5f;
    public Animator Anim;
    public float jumpHeight;

    void Awake()
    {
        Anim = meshTransform.GetComponent<Animator>();
        charSound = GetComponent<CharSound>();
        var gameplayActionMap = inputActions.FindActionMap("Player");
        moveAction = gameplayActionMap.FindAction("Move");
        camTurnLAction = gameplayActionMap.FindAction("CamTurnLeft");
        camTurnRAction = gameplayActionMap.FindAction("CamTurnRight");
        jumpAction = gameplayActionMap.FindAction("Jump");
        normalState = new NormalState(this);
        rotationState = new RotationState(this);
        SwitchState(normalState);
    }

    void Update()
    {
        currentState.OnUpdate();
    }

    public void SwitchState(CCState state)
    {
        currentState?.OnStateExit();
        currentState = state;
        currentState.OnStateEnter();
    }
}
