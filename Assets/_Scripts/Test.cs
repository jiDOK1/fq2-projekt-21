using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    void Start()
    {
        Material mat = GetComponent<MeshRenderer>()?.material;
        mat?.SetColor("diffuse", Color.red);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
